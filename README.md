# EasyConduite #

EasyConduite helps small drama troupe stage manager to play several audio tracks during the drama, at right time.

## Features ##
* Loading several audio.
* Assign it to a key of keyboard for play/stop with.
*  Adjust volume during playing.

### TODO ###
 * Manage audio tracks on a drama timeline.
 * Message to inform stage manager few minutes before playing.
 * Expose by grizzly server for REST command.


For cross-platform, I chose Java FX and Java 8

### Screenshot ###

![easyconduitemain01.png](https://bitbucket.org/repo/jXooyj/images/1944856645-easyconduitemain01.png)

![easyconduiteconf01.png](https://bitbucket.org/repo/jXooyj/images/823895134-easyconduiteconf01.png)